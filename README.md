# M1 Arcade Music Player

[M1] is a program that lets you play sounds and music from over 1,400 arcade 
and pinball games and record them to `.wav` files.

**Note:**
This repository is for reference and is not meant to be commited to. It's only
here because it was (almost) impossible to find the source code for this
program.

[M1]: http://rbelmont.mameworld.info/?page_id=223
